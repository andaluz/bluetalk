package com.appplanet.app.bluetalk.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {
    public final static String TAG = "MyBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Bluetalk Broadcast Message Received", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onReceive() is called");
    }
}
