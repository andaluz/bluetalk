package com.appplanet.app.bluetalk;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.appplanet.app.bluetalk.broadcast.MyBroadcastReceiver;
import com.appplanet.app.bluetalk.controller.BluetoothController;

public class MainActivity extends AppCompatActivity {
    public final static String TAG = "MainActivity";

    private Button startButton;
    private BluetoothController blu2Controller;
    private MyBroadcastReceiver myBCReceiver;
    private LocalBroadcastManager localBCManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        initializeUI();
        intializeBroadcasts();
    }

    private void intializeBroadcasts() {
        localBCManager = LocalBroadcastManager.getInstance(this);
        myBCReceiver = new MyBroadcastReceiver();
    }

    private void initializeUI() {
        startButton = (Button) findViewById(R.id.btnStart);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Button start pressed!");

                Intent intent = new Intent();
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setAction("com.appplanet.app.bluetalk.mybroadcast"/*getResources().getString(R.string.mybroadcast)*/);
                if(localBCManager != null)
                        localBCManager.sendBroadcast(intent);
                else
                    Log.d(TAG, "localBCManager is null!");
            }
        });

        //blu2Controller = new BluetoothController(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case BluetoothController.REQUEST_ENABLE_BT:
                break;
            default:
                Log.d(TAG, "Received request code: " + requestCode);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.appplanet.app.bluetalk.mybroadcast");
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        localBCManager.registerReceiver(myBCReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();

        localBCManager.unregisterReceiver(myBCReceiver);
    }
}
