package com.appplanet.app.bluetalk.controller;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;

import java.lang.ref.WeakReference;

public class BluetoothController {

    // Get the default adapter
    BluetoothAdapter mBluetoothAdapter;
    private final WeakReference<Activity> mActvity;

    public final static int REQUEST_ENABLE_BT = 0xB1002;    // B1002 <=> BLOOTHOO ;-)
    private boolean blu2Activated = false;


    public BluetoothController(Activity activity) {
        mActvity = new WeakReference<>(activity);
    }

    public void initializeBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null) {
            // Device doesn't support Bluetooth
        }
    }

    /**
     *
     */
    public void activateBluetooth() {
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            Activity activity = mActvity.get();
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    private BluetoothProfile.ServiceListener mProfileListener = new BluetoothProfile.ServiceListener() {
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            if (profile == BluetoothProfile.SAP) {
                // What is SAP profile?
            }
        }
        public void onServiceDisconnected(int profile) {
            if (profile == BluetoothProfile.SAP) {
                //mBluetoothHeadset = null;
            }
        }
    };

    public void setBluetoothResult(boolean success) {
        if(success) {

        } else {

        }

        blu2Activated = success;
    }

}
